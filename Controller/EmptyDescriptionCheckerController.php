<?php
/*
 * This file is part of the EmptyDescriptionCheckerBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace KimaiPlugin\EmptyDescriptionCheckerBundle\Controller;

use App\Controller\AbstractController;
use App\Entity\Timesheet;
use App\Entity\User;
use App\Repository\Query\TimesheetQuery;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\ORMException;
use Exception;
use KimaiPlugin\EmptyDescriptionCheckerBundle\Repository\EmptyDescriptionCheckerRepository;
use App\Mail\KimaiMailer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * @Route(path="/empty-description-checker")
 * @Security("is_granted('ROLE_SUPER_ADMIN') or is_granted('empty_description_checker')")
 */
class EmptyDescriptionCheckerController extends AbstractController
{
    /**
     * @var EmptyDescriptionCheckerRepository
     */
    private $repository;
    /**
     * @var KimaiMailer
     */
    private $mailer;

    /**
     * @var Translator
     */
    private $translator;


    /**
     * EmptyDescriptionCheckerController constructor.
     * @param EmptyDescriptionCheckerRepository $repository
     * @param KimaiMailer $mailer
     * @param TranslatorInterface $translator
     */
    public function __construct(EmptyDescriptionCheckerRepository $repository, KimaiMailer $mailer, TranslatorInterface $translator)
    {
        $this->repository = $repository;
        $this->mailer = $mailer;
        $this->translator = $translator;
    }

    /**
     * @Route(path="", defaults={"page": 1}, name="empty_description_checker", methods={"GET"})
     * @Route(path="/page/{page}", requirements={"page": "[1-9]\d*"}, name="empty_description_checker_paginated", methods={"GET"})
     *
     * @param int $page
     * @return Response
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function indexAction($page)
    {
        $timeSheetQuery = new TimesheetQuery();
        $timeSheetQuery->setPage($page);

        $entries = $this->repository->getAllEmptyDescriptionsPaginated($timeSheetQuery);

        return $this->render('@EmptyDescriptionChecker/index.html.twig', [
            'entries' => $entries,
            'page' => $page
        ]);
    }

    /**
     * @Route(path="/send-emails", name="empty_description_checker_send_emails", methods={"GET"})
     *
     * @param bool $returnView
     * @return int|Response
     * @throws Exception
     * @throws TransportExceptionInterface
     */
    public function sendEmailsAction(bool $returnView = true)
    {
        $entriesToSendViaMail = $this->getEntriesToSendViaMail();

        if (!empty($entriesToSendViaMail)) {
            $this->sendEntriesWithoutDescriptionToUser($entriesToSendViaMail);
        }

        if ($returnView) {
            return $this->render('@EmptyDescriptionChecker/sendEmails.html.twig', []);
        };

        return 0;
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getEntriesToSendViaMail(): array
    {
        /** @var Timesheet $entriesToSend */
        $entriesToSend = $this->repository->getAllEmptyDescriptions();

        if (!empty($entriesToSend)) {
            $sortedEntriesToSend = [];

            /** @var Timesheet $entryToSend */
            foreach ($entriesToSend AS $entryToSend) {
                $userId = $entryToSend->getUser()->getId();
                $sortedEntryToSend = [
                    'begin' => (new DateTime())->setTimestamp($entryToSend->getBegin()->getTimestamp()),
                    'end' => (new DateTime())->setTimestamp($entryToSend->getEnd()->getTimestamp()),
                    'project' => $entryToSend->getProject()->getName(),
                    'activity' => $entryToSend->getActivity()->getName()
                ];

                if (!array_key_exists($userId, $sortedEntriesToSend)) {
                    $sortedEntriesToSend[$entryToSend->getUser()->getId()] = [];
                }
                array_push($sortedEntriesToSend[$userId], $sortedEntryToSend);
            }

            return $sortedEntriesToSend;
        }

        return [];
    }

    /**
     * @param array $entries
     * @throws TransportExceptionInterface
     */
    private function sendEntriesWithoutDescriptionToUser(array $entries)
    {
        foreach ($entries AS $userId => $allEntriesOfUser) {
            /** @var User $user */
            $user = $this->getDoctrine()->getRepository(User::class)->find($userId);

            $tempAdminList = $this->getDoctrine()->getRepository(User::class)->findAll();
            $adminList = array();
            foreach ($tempAdminList as $key => $value) {
                if ($value->isAdmin()) {
                    $adminList[] = $value->getEmail();
                }
            }

            $ccList = '';
            if (sizeof($adminList) > 0) {
                $ccList = implode(',', $adminList);
            }

            $context = array('allEntriesOfUser' => $allEntriesOfUser, 'locale' => $this->translator->setLocale($user->getLocale()));

            $this->sendEmail($user->getEmail(), $this->translator->trans('emptydescriptionchecker.email.subject'), '@EmptyDescriptionChecker/emailWithEmptyDescriptions.html.twig', $context, $ccList);
        }
    }

    /**
     * @param string $to
     * @param string $subject
     * @param string $htmlTemplate
     * @param array $context
     * @param string $ccList
     * @throws TransportExceptionInterface
     */
    private function sendEmail(string $to, string $subject, string $htmlTemplate, array $context, string $ccList)
    {
        try {
            if ($to != "") {
                $email = (new TemplatedEmail())
                    ->to(new Address($to))
                    ->subject($subject)
                    ->htmlTemplate($htmlTemplate)
                    ->context($context);

                if ($ccList != '') {
                    $listCC = explode(',', $ccList);

                    foreach ($listCC as $key => $value) {
                        $email->addCc($value);
                    }
                }

                $this->mailer->send($email);
            }
        } catch (ORMException $ex) {
            $this->flashError('action.update.error', ['%reason%' => $ex->getMessage()]);
        }
    }
}
